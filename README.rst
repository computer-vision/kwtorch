The kwtorch Module
==================

|Pypi| |PypiDownloads|

Currently a work in progress for porting useful parts of netharn.



.. |Pypi| image:: https://img.shields.io/pypi/v/kwtorch.svg
    :target: https://pypi.python.org/pypi/kwtorch

.. |PypiDownloads| image:: https://img.shields.io/pypi/dm/kwtorch.svg
    :target: https://pypistats.org/packages/kwtorch
